---
title: Vysakh Premkumar 
toc: true 
---
![](/Pages/Home/debconf23_group.jpg)
I'm an engineering student based in Calicut. My focus area for the past few years has been GNU /Linux computing and decentralized social networks. While you're here try and find me in this group photo of [DebConf23](https://debconf23.debconf.org/)
## Explore

{{< cards >}}
  {{< card link="docs" title="Posts" icon="book-open" >}}
  {{< card link="about" title="About" icon="user" >}}
  {{< card link="sysinfo" title="System Info" icon="desktop-computer" >}}
{{< /cards >}}

## Things I'm workin on RN.

{{< cards >}}
  {{< card link="https://wikinitc.kenobi.win" title="WikiNITC" icon="user-group" >}}
  {{< card link="https://newsletter.fosscell.org" title="Newsletter" icon="rss" >}}
  {{< card link="https://diasp.in" title="Diasp.in" icon="bookmark" >}}
{{< /cards >}}


